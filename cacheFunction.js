module.exports = function cacheFunction(cb) {
    const cache = {};
    return function (parameters) {
        if (!Object.prototype.hasOwnProperty.call(cache, parameters)) {
            cache[parameters] = cb(parameters);
            return cb(parameters);
        }
        else
            return { ["cache stored values"]: cache[parameters] };
    }
}
