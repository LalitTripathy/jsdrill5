module.exports = function counterFactory() {
    let counter = 0;
    function increment() {
        return ++counter;
    }
    function decrement() {
        return --counter;
    }
    return {
        increment,
        decrement
    }
}
