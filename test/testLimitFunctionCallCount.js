limitFunctionCallCount = require("../limitFunctionCallCount.js")
let count1 = 0;

function cb_1() {
    count1++;
    return (count1);
}

let count2 = 0;

function cb_2() {
    count2++;
    return (count2);
}

let lower = limitFunctionCallCount(cb_1, 4);
let upper = limitFunctionCallCount(cb_2, 4);

lower();
lower();
upper();
upper();
upper();
upper();
upper();
upper();

let orginalResult1 = lower();
let orginalResult2 = upper();
let expectedRes1 = 3;
let expectedRes2 = null;

if ((orginalResult1 === expectedRes1) && (orginalResult2 === expectedRes2)) {
    console.log(`lower invoked ${count1} times & upper invoked ${count2} times`);
}
else {
    console.log("Results not same.");
}
