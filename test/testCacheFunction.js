const cacheFunction = require('../cacheFunction.js')
let cb = a => a + 20;
let usedArgument = cacheFunction(cb);
usedArgument(10);
usedArgument(20)
usedArgument(10);
usedArgument(10);
usedArgument(20);
let result = usedArgument(20);
let expectedResult = { 'cache stored values': 40 }
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log(result);
} else {
    console.log("No Same result");
}
